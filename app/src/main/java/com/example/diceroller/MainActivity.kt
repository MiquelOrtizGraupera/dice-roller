package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    //lateinit var resultTextView: TextView
    //lateinit var resultTextView2: TextView
    lateinit var dice1:ImageView
    lateinit var dice2:ImageView
    lateinit var resetButton:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        val mySnackbar = Snackbar.make()
        rollButton = findViewById(R.id.roll_button)
        //resultTextView = findViewById(R.id.text_dice)
        //resultTextView2 = findViewById(R.id.text_dice2)

        dice1 = findViewById(R.id.dado1)
        dice2 = findViewById(R.id.dado2)

        val diceSides = arrayOf(
            R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,
            R.drawable.dice_5,R.drawable.dice_6)


        rollButton.setOnClickListener {
            var numero : Int = (1..6).random()
            var numero2 :Int=(1..6).random()

            dice1.setImageDrawable(getDrawable(diceSides[numero-1]))
            dice2.setImageDrawable(getDrawable(diceSides[numero2-1]))
        }

        resetButton = findViewById(R.id.reset)
        resetButton.setOnClickListener {
            dice1.setImageResource(R.drawable.empty_dice)
            dice2.setImageResource(R.drawable.empty_dice)
        }

    }
}
